package ie.ucd.drink;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

/**
 * this class NotDrinker extends Person specify a Notdrinker just can drink
 * NotAlcoholic Drink
 * 
 * @author zhuwe
 *
 */
public class NotDrinker extends Person {

	private boolean anyFood;

	/**
	 * constructor
	 */
	public NotDrinker() {
		// TODO Auto-generated constructor stub
		this.anyFood = true;
	}

	/**
	 * return true if drink is NotAlcoholic Drink
	 */
	@Override
	public boolean drink(Drink drink) {
		// TODO Auto-generated method stub

		if (drink instanceof AlcoholicDrink) {
			System.out.println("Sorry, NotDrinker can't drink Alcoholic drink");
			return false;
		}
		return true;

	}

	@Override
	public boolean eat(Food food) {
		// TODO Auto-generated method stub
		return anyFood;
	}

}
