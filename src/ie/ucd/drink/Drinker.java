package ie.ucd.drink;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

/**
 * this class Drinker extends Person specify a drinker can drink both alcoholic
 * Drink and NotAlcoholic Drink
 * 
 * @author zhuwe
 *
 */

public class Drinker extends Person {

	private boolean anyFood;
	private int numberOfDrinks;

	/**
	 * constructor
	 * 
	 * @param weight
	 */
	public Drinker(double weight) {
		super();
		this.anyFood = true;
		this.numberOfDrinks = 0;
		this.setWeight(weight);
	}

	/**
	 * return true if drink is either NotAlcoholic or Alcoholic drink
	 */
	@Override
	public boolean drink(Drink drink) {
		// TODO Auto-generated method stub
		if (drink instanceof AlcoholicDrink)
			numberOfDrinks += 1;
		System.out.print("numberOfDrinks:" + numberOfDrinks + "\n");
		return true;
	}

	@Override
	public boolean eat(Food food) {
		// TODO Auto-generated method stub
		return anyFood;
	}

	/**
	 * 
	 * @return true if numberOfDrinks is bigger than person's weight divided by 10
	 */
	public boolean isDrunk() {

		return (this.numberOfDrinks) > (getWeight() / 10);

	}

}
