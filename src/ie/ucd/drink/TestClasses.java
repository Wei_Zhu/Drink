package ie.ucd.drink;

import ie.ucd.items.Drink;
import ie.ucd.items.Wine;
import ie.ucd.people.Person;

/**
 * Test to see that classes are working properly and that isDrunk() works
 * 
 * @author zhuwe
 *
 */
public class TestClasses {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* Initialise Drink and Person and Drinker */
		Drink sprintwater = new SpringWater("nofushanquan", 10);
		Drink cococola = new Cola("cococola", 10);
		Drink wine = new Wine("whisky", 20, 40, null);
		Person notDrinker = new NotDrinker();
		Drinker drinker = new Drinker(55);

		/* Test correct implementation of class notDrinker */
		System.out.print("Testing correct implementation of class notDrinker.\n");
		System.out.print("drink: " + notDrinker.drink(wine) + ".\neat: " + notDrinker.eat(null) + "\n\n");

		/* Test correct implementation of class Drinker */
		System.out.print("Testing correct implementation of class Drinker.\n");
		System.out.print("drink: " + drinker.drink(wine) + ".\neat: " + drinker.eat(null) + "\n\n");

		/*
		 * Test to see whether notDrinker is allowed to drink Alcoholic Drink or
		 * notAlcoholic Drink
		 */
		notDrinker.drink(wine);
		notDrinker.drink(cococola);
		notDrinker.drink(sprintwater);

		/*
		 * Test to see Drinker is allowed to drink both Alcoholic Drink and notAlcoholic
		 * Drink
		 */
		drinker.drink(wine);
		drinker.drink(cococola);
		drinker.drink(sprintwater);

		/* Test to see if Drinker is isDrunk */
		boolean isdrunk = drinker.isDrunk();
		System.out.print("drinker is drunk? " + isdrunk + "\n\n");
		drinker.drink(wine);
		drinker.drink(wine);
		drinker.drink(wine);
		System.out.print("drinker is drunk? " + drinker.isDrunk() + "\n\n");
		drinker.drink(wine);
		System.out.print("drinker is drunk? " + drinker.isDrunk() + "\n\n");

		System.out.print("******* End of the program *********");
	}

}
