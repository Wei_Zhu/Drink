package ie.ucd.drink;

import ie.ucd.items.NotAlcoholicDrink;

public class SpringWater extends NotAlcoholicDrink{
	
	private String springWaterName;
	private double springWatervolume;
	
	public SpringWater(String name, double volume) {
		super(name, volume);
		// TODO Auto-generated constructor stub
		this.setSpringWaterName(name);
		this.setSpringWatervolume(volume);
		
	}

	public String getSpringWaterName() {
		return springWaterName;
	}

	public void setSpringWaterName(String springWaterName) {
		this.springWaterName = springWaterName;
	}

	public double getSpringWatervolume() {
		return springWatervolume;
	}

	public void setSpringWatervolume(double springWatervolume) {
		this.springWatervolume = springWatervolume;
	}
	

}
