package ie.ucd.drink;

import ie.ucd.items.NotAlcoholicDrink;

public class Cola extends NotAlcoholicDrink {

	private String colaName;
	private double colaVolume;

	/**
	 * constructor
	 * 
	 * @param name
	 * @param volume
	 */
	public Cola(String name, double volume) {
		super(name, volume);
		// TODO Auto-generated constructor stub
		this.setColaName(name);
		this.setColaVolume(volume);

	}

	public String getColaName() {
		return colaName;
	}

	public void setColaName(String colaName) {
		this.colaName = colaName;
	}

	public double getColaVolume() {
		return colaVolume;
	}

	public void setColaVolume(double colaVolume) {
		this.colaVolume = colaVolume;
	}

}
